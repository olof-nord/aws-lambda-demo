import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.lambda.runtime.RequestStreamHandler
import com.google.gson.GsonBuilder
import java.io.InputStream
import java.io.OutputStream
import java.io.OutputStreamWriter

data class HandlerInput(val who: String)
data class HandlerOutput(val message: String)

@Suppress("unused")
class Main: RequestStreamHandler {
    private val gson by lazy { GsonBuilder().create() }

    override fun handleRequest(input: InputStream?, output: OutputStream, context: Context?) {

        val requestData = input?.reader().use {
            gson.fromJson(it, HandlerInput::class.java)
        } ?: throw Exception()

        val response = HandlerOutput("Hello ${requestData.who}")

        OutputStreamWriter(output, "UTF-8").use {
            it.write(gson.toJson(response))
        }
    }
}