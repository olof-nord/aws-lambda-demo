# aws-lambda-demo

Kotlin AWS Lambda demo

This is a demo project to implement and deploy an AWS Lambda event handler.

## Setting it up

This is based on using the AWS CLI.

The initial configuration of the CLI can be done with `aws configure`.

## Build

We need to build a jar to deploy your function to AWS.

```shell
$ gradle clean build shadowJar
```

## Deploy

The first time we deploy the function, we use `create-function`.

Note that the user id and role needs to be adjusted.

```shell
$ aws lambda create-function \
--function-name kotlin-hello \
--zip-file fileb://build/libs/aws-lambda-demo-0.1.0-all.jar \
--role arn:aws:iam::<numeric user id>:role/<AWS IAM role> \
--handler Main::handleRequest --runtime java11 \
--timeout 15 --memory-size 128
```

## Update

For the subsequent function updates, we use `update-function-code`.

```shell
$ aws lambda update-function-code --function-name kotlin-hello --zip-file fileb://build/libs/aws-lambda-demo-0.1.0-all.jar
```

## Execute

When executing the function, we use `invoke`.

```shell
$ aws lambda invoke --function-name kotlin-hello --payload '{"who": "AWS Fan"}' output.json
$ cat output.json
```

## Resources

* [Using Lambda with the AWS CLI](https://docs.aws.amazon.com/lambda/latest/dg/gettingstarted-awscli.html)
* [Kotlin and Groovy JVM Languages with AWS Lambda](https://aws.amazon.com/blogs/compute/kotlin-and-groovy-jvm-languages-with-aws-lambda/)
* [Write an AWS Lambda Function with Kotlin and Micronaut](https://www.raywenderlich.com/5777183-write-an-aws-lambda-function-with-kotlin-and-micronaut)
* [AWS Lambda + Kotlin -> Redirector](https://medium.com/asics-digital/aws-lambda-kotlin-redirector-4d666d49b603)
* [Creating an AWS Lambda Kotlin function](https://dev.to/gautemeekolsen/creating-an-aws-lambda-kotlin-function-3j80)
